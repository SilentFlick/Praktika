#version 330 core

layout( location = 0 ) in vec3  vPosition;
layout( location = 1 ) in vec4 vColor;
layout( location = 2) in vec2 vTexture;

out vec4 color;
out vec2 texCoord;

void main() {
   gl_Position = vec4(vPosition, 1.0);
   color = vColor;
   texCoord = vec2(vTexture.x, vTexture.y);
}
