CC = g++
# Install GL, GLEW, GLFW, glm and FreeImage libraries.
GLINC = /usr/include/GL
GLFWINC = /usr/include/GLFW
INCS = -I$(GLINC) -I$(GLFWINC)
LIBS = -lGL -lGLEW -lglfw
OSGLIBS = -lOpenThreads -losg -losgDB -losgUtil -losgViewer
CFLAGS = -pedantic -g -Wall -Os $(INCS)
LDFLAGS = $(LIBS)

OBJ = $(wildcard *.o)
BIN = $(wildcard cube triangles rectangle brick osgHello osgShape osgColorQuad)

all: cube triangles rectangle osgHello osgShape brick

options:
	@echo build options:
	@echo "CFLAGS   = $(CFLAGS)"
	@echo "LDFLAGS  = $(LDFLAGS)"
	@echo "CC       = $(CC)"

.c.o:
	$(CC) -c $(CFLAGS) $<

osgColorQuad: osgColorQuad.o
	$(CC) -o $@ osgColorQuad.o $(OSGLIBS)

osgShape: osgShape.o
	$(CC) -o $@ osgShape.o $(OSGLIBS)

osgHello: osgHello.o
	$(CC) -o $@ osgHello.o $(OSGLIBS)

rectangle: rectangle.o window.o shader.o
	$(CC) -o $@ rectangle.o window.o shader.o $(LDFLAGS)

brick: brick.o window.o shader.o
	$(CC) -o $@ brick.o window.o shader.o $(LDFLAGS)

cube: cube.o window.o shader.o
	$(CC) -o $@ cube.o window.o shader.o $(LDFLAGS)

triangles: triangles.o window.o shader.o
	$(CC) -o $@ triangles.o window.o shader.o $(LDFLAGS)

clean:
	@echo clean
	rm -f ${OBJ} *.log a.out ${BIN}

.PHONY: all options clean cube triangles rectangle osgHello osgShape osgColorQuad
