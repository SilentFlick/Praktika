#version 450 core

uniform vec3 brickColor, mortarColor;
uniform vec3 brickSize;
uniform vec3 brickPct;

in vec3  MCPosition;

layout (location = 0 )out vec4 fColor;

void main()
{
    vec3 color;
    vec3 position, useBrick;

    position = MCPosition / brickSize;
    if (fract(position.y * 0.5) > 0.5) {
        position.x += 0.5;
    }

    position = fract(position);
    useBrick = step(position, brickPct);

    color = mix(mortarColor, brickColor, useBrick.x * useBrick.y);
    color *= vec3(1.0);

    fColor = vec4(color, 1.0);
}
