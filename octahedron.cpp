#include "shader.h"
#include "window.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#define NumVertices 8

enum VAO_IDs { Cubes, NumVAOs };
enum Buffer_IDs { ArrayBuffer, IndexBuffer, NumBuffers };
enum Attrib_IDs { vPosition, vColor, vNormal };

GLuint VAOs[NumVAOs];
GLuint Buffers[NumBuffers];
GLuint program;

float angle = 0.0;

void setModelViewProjection(void)
{
    GLuint mvpMatrixUnif = glGetUniformLocation(program, "mvpMatrix");

    glm::mat4 model = glm::mat4(1.0); // initialize as identity matrix
    glm::mat4 view = glm::lookAt(glm::vec3(2.0, 2.0, 2.0),  // camera position
                                 glm::vec3(0.0, 0.0, 0.0),  // camera target
                                 glm::vec3(0.0, 1.0, 0.0)); // up vector
    glm::mat4 projection = glm::perspective(glm::radians(60.0), // field of view
                                            1.0,                // aspect ratio
                                            0.1,                // near plane
                                            100.0);             // far plane

    glm::mat4 matrix = projection * view * model;

    glUniformMatrix4fv(mvpMatrixUnif, 1.0, GL_FALSE, glm::value_ptr(matrix));
}

void setOffset(GLuint program, float x, float y, float z)
{
    GLint offsetUnif = glGetUniformLocation(program, "offset");
    glUniform3f(offsetUnif, x, y, z);
}

void rotateCube(void)
{
    angle += 10.0f;
    GLuint    transMatrixUnif = glGetUniformLocation(program, "transMatrix");
    glm::mat4 rotation = glm::rotate(glm::mat4(1.0), glm::radians(angle),
                                     glm::vec3(0.0, 1.0, 0.0));
    glUniformMatrix4fv(transMatrixUnif, 1.0, GL_FALSE,
                       glm::value_ptr(rotation));
}

void init(void)
{
    ShaderInfo shaders[] = {
        {GL_VERTEX_SHADER,   "cube.vert"},
        {GL_FRAGMENT_SHADER, "cube.frag"},
        {GL_NONE,            NULL       }
    };
    program = loadShaders(shaders);
    glUseProgram(program);

    setOffset(program, 0.0, 0.0, 0.0);
    setModelViewProjection();
    rotateCube();

    glGenVertexArrays(NumVAOs, VAOs);
    glBindVertexArray(VAOs[Cubes]);

    GLfloat vertices[NumVertices][9] = {
        {-0.5, -0.5, 0.5,  1.0, 1.0, 1.0, 0.0, 0.0, 1.0 },
        {0.5,  -0.5, 0.5,  1.0, 1.0, 0.0, 0.0, 0.0, 1.0 },
        {0.5,  0.5,  0.5,  1.0, 0.0, 1.0, 0.0, 0.0, 1.0 },
        {-0.5, 0.5,  0.5,  1.0, 0.0, 0.0, 0.0, 0.0, 1.0 },
        {-0.5, -0.5, -0.5, 0.0, 1.0, 1.0, 0.0, 0.0, -1.0},
        {0.5,  -0.5, -0.5, 0.0, 1.0, 0.0, 0.0, 0.0, -1.0},
        {0.5,  0.5,  -0.5, 0.0, 0.0, 1.0, 0.0, 0.0, -1.0},
        {-0.5, 0.5,  -0.5, 0.5, 0.5, 0.5, 0.0, 0.0, -1.0}
    };
    GLushort indices[36] = {0, 1, 2, 2, 3, 0, 4, 5, 6, 6, 7, 4,
                            3, 2, 6, 6, 7, 3, 0, 1, 5, 5, 4, 0,
                            1, 2, 6, 6, 5, 1, 0, 3, 7, 7, 4, 0};

    glCreateBuffers(NumBuffers, Buffers);
    setImmuBufferObject(Buffers[ArrayBuffer], GL_ARRAY_BUFFER, sizeof(vertices),
                        vertices, GL_MAP_WRITE_BIT);
    setImmuBufferObject(Buffers[IndexBuffer], GL_ELEMENT_ARRAY_BUFFER,
                        sizeof(indices), indices, GL_MAP_READ_BIT);

    setVertexAttribute(Buffers[ArrayBuffer], ArrayBuffer, vPosition, 3,
                       GL_FLOAT, GL_FALSE, 9 * sizeof(GL_FLOAT), 0, 0);
    setVertexAttribute(Buffers[ArrayBuffer], ArrayBuffer, vColor, 3, GL_FLOAT,
                       GL_FALSE, 9 * sizeof(GL_FLOAT), 0, 3 * sizeof(GL_FLOAT));
    setVertexAttribute(Buffers[ArrayBuffer], ArrayBuffer, vNormal, 3, GL_FLOAT,
                       GL_FALSE, 9 * sizeof(GL_FLOAT), 0, 6 * sizeof(GL_FLOAT));

    glBindVertexArray(VAOs[Cubes]);

    GLuint normalMatrixUnif = glGetUniformLocation(program, "normalMatrix");
    glUniformMatrix3fv(normalMatrixUnif, 1.0, GL_FALSE,
                       glm::value_ptr(glm::mat3(1.0)));
    setLight();
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);
    glDisable(GL_CULL_FACE);
    glCullFace(GL_BACK);
    glFrontFace(GL_CW);
}

float random(int max) { return (float)rand() / (float)(RAND_MAX / max); }

void setRandomColor(void)
{
    GLfloat *bufferData = (GLfloat *)glMapNamedBufferRange(
        Buffers[ArrayBuffer], 0, NumVertices * 9 * sizeof(GLfloat),
        GL_MAP_WRITE_BIT);
    for (int i = 0; i < NumVertices; i++) {
        size_t colorOffset = i * 9 + 3;
        bufferData[colorOffset] = random(1);
        bufferData[colorOffset + 1] = random(1);
        bufferData[colorOffset + 2] = random(1);
    }
    glUnmapNamedBuffer(Buffers[ArrayBuffer]);
}

void display(void)
{
    double time = glfwGetTime();
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glClearBufferfv(GL_COLOR, 0, black);
    // glBindVertexArray(VAOs[Cubes]);
    glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_SHORT, NULL);
    // glDrawElementsInstanced(GL_TRIANGLES, 36, GL_UNSIGNED_SHORT, NULL, 1);
    if (time >= 0.1) {
        setRandomColor();
        rotateCube();
        glfwSetTime(0.0);
    }
    glFlush();
}

void input(GLFWwindow *window, int key, int scancode, int action, int mods)
{
    if (action == GLFW_PRESS) {
        switch (key) {
        case GLFW_KEY_ESCAPE:
            glfwSetWindowShouldClose(window, 1);
            break;
        default:
            break;
        }
    }
}

int main(void)
{
    // setWindowSize(812, 812);
    createWindow("Cube", NULL, NULL, init, display, input);
    return 0;
}
