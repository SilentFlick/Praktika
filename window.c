#include "window.h"

static int mainWidth = 800;
static int mainHeight = 800;

void error_callback(int error, const char *description)
{
    printf("Error: %d - '%s'\n", error, description);
    exit(error);
}

void framebuffer_callback(GLFWwindow *window, int width, int height)
{
    glViewport(0, 0, width, height);
}

void windowSize_callback(GLFWwindow *window, int width, int height)
{
    mainWidth = width;
    mainHeight = height;
    glfwSetWindowSize(window, width, height);
}

void setWindowSize(int width, int height)
{
    mainWidth = width;
    mainHeight = height;
}

int getWidth() { return mainWidth; }

int getHeight() { return mainHeight; }

/* @Param title: the initial UTF-8 encoded window title
 * @Param monitor: the monitor use for full screen mode, or NULL for windowed
 * mode
 * @Parma share: the window whose context to share resources with, or NULL to
 * not share resources
 * @Param init: pointer to function init which has a void return type and no
 * parameter
 * @Param display: pointer to function display which has a void return type and
 * no parameter
 * @Param input: pointer to function input which has a void return type and can
 * be set to receive all the various kinds of events. */
void createWindow(const char *title, GLFWmonitor *monitor, GLFWwindow *share,
                  void (*init)(void), void (*display)(void),
                  void (*input)(GLFWwindow *window, int key, int scancode,
                                int action, int mods))
{
    glfwSetErrorCallback(error_callback);
    if (!glfwInit()) {
        exit(EXIT_FAILURE);
    }
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    GLFWwindow *mainWindow =
        glfwCreateWindow(mainWidth, mainHeight, title, monitor, share);
    if (!mainWindow) {
        glfwTerminate();
        exit(EXIT_FAILURE);
    }
    if (input) {
        glfwSetKeyCallback(mainWindow, input);
    }
    glfwMakeContextCurrent(mainWindow);
    glfwSwapInterval(1);
    glfwSetWindowSizeCallback(mainWindow, windowSize_callback);
    glfwSetFramebufferSizeCallback(mainWindow, framebuffer_callback);
    glewExperimental = GL_TRUE;
    if (glewInit()) {
        perror("glewInit");
        exit(EXIT_FAILURE);
    }
    printf("OpenGL %s (Vendor %s, %s), GLSL %s\n", glGetString(GL_VERSION),
           glGetString(GL_VENDOR), glGetString(GL_RENDERER),
           glGetString(GL_SHADING_LANGUAGE_VERSION));
    if (init) {
        init();
        while (!glfwWindowShouldClose(mainWindow)) {
            if (display) {
                display();
            }
            glfwSwapBuffers(mainWindow);
            glfwPollEvents();
        }
    } else {
        perror("init");
    }
    glfwDestroyWindow(mainWindow);
    glfwTerminate();
}
