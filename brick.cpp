#include "shader.h"
#include "window.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#define NumVertices 8

enum VAO_IDs { Cubes, NumVAOs };
enum Buffer_IDs { ArrayBuffer, IndexBuffer, NumBuffers };
enum Attrib_IDs { vPosition };

GLuint VAOs[NumVAOs];
GLuint Buffers[NumBuffers];
GLuint program;

void setModelViewProjection(void)
{
    GLuint mvpMatrixUnif = glGetUniformLocation(program, "mvpMatrix");

    glm::mat4 model = glm::mat4(1.0); // initialize as identity matrix
    glm::mat4 view = glm::lookAt(glm::vec3(2.0, 2.0, 2.0),  // camera position
                                 glm::vec3(0.0, 0.0, 0.0),  // camera target
                                 glm::vec3(0.0, 1.0, 0.0)); // up vector
    glm::mat4 projection = glm::perspective(glm::radians(60.0), // field of view
                                            1.0,                // aspect ratio
                                            0.1,                // near plane
                                            100.0);             // far plane

    glm::mat4 matrix = projection * view * model;

    glUniformMatrix4fv(mvpMatrixUnif, 1.0, GL_FALSE, glm::value_ptr(matrix));
}

void setOffset(GLuint program, float x, float y, float z)
{
    GLint offsetUnif = glGetUniformLocation(program, "offset");
    glUniform3f(offsetUnif, x, y, z);
}

void rotateCube(void)
{
    float     angle = 90.0;
    GLuint    transMatrixUnif = glGetUniformLocation(program, "transMatrix");
    glm::mat4 rotation = glm::rotate(glm::mat4(1.0), glm::radians(angle),
                                     glm::vec3(0.0, 1.0, 0.0));
    glUniformMatrix4fv(transMatrixUnif, 1.0, GL_FALSE,
                       glm::value_ptr(rotation));
}

void setBrick(void)
{
    GLuint brickColorUnif = glGetUniformLocation(program, "brickColor");
    GLuint mortarColorUnif = glGetUniformLocation(program, "mortarColor");
    GLuint brickSizeUnif = glGetUniformLocation(program, "brickSize");
    GLuint brickPctUnif = glGetUniformLocation(program, "brickPct");

    glUniform3f(brickColorUnif, 0.8f, 0.8f, 0.8f);
    glUniform3f(mortarColorUnif, 0.3f, 0.3f, 0.3f);
    glUniform3f(brickSizeUnif, 0.1f, 0.1f, 0.1f);
    glUniform3f(brickPctUnif, 0.9f, 0.9f, 0.1f);
}

void init(void)
{
    ShaderInfo shaders[] = {
        {GL_VERTEX_SHADER,   "brick.vert"},
        {GL_FRAGMENT_SHADER, "brick.frag"},
        {GL_NONE,            NULL        }
    };
    program = loadShaders(shaders);
    glUseProgram(program);

    setOffset(program, 0.0, 0.0, 0.0);
    setModelViewProjection();
    rotateCube();

    glGenVertexArrays(NumVAOs, VAOs);
    glBindVertexArray(VAOs[Cubes]);

    GLfloat vertices[NumVertices][3] = {
        {-0.5, -0.5, 0.5 },
        {0.5,  -0.5, 0.5 },
        {0.5,  0.5,  0.5 },
        {-0.5, 0.5,  0.5 },
        {-0.5, -0.5, -0.5},
        {0.5,  -0.5, -0.5},
        {0.5,  0.5,  -0.5},
        {-0.5, 0.5,  -0.5}
    };
    GLushort indices[36] = {0, 1, 2, 2, 3, 0, 4, 5, 6, 6, 7, 4,
                            3, 2, 6, 6, 7, 3, 0, 1, 5, 5, 4, 0,
                            1, 2, 6, 6, 5, 1, 0, 3, 7, 7, 4, 0};

    glCreateBuffers(NumBuffers, Buffers);
    setImmuBufferObject(Buffers[ArrayBuffer], GL_ARRAY_BUFFER, sizeof(vertices),
                        vertices, GL_MAP_WRITE_BIT);
    setImmuBufferObject(Buffers[IndexBuffer], GL_ELEMENT_ARRAY_BUFFER,
                        sizeof(indices), indices, GL_MAP_READ_BIT);

    setVertexAttribute(Buffers[ArrayBuffer], ArrayBuffer, vPosition, 3,
                       GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), 0, 0);

    setBrick();
    glBindVertexArray(VAOs[Cubes]);

    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);
    glDisable(GL_CULL_FACE);
    glCullFace(GL_BACK);
    glFrontFace(GL_CW);
}

void display(void)
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glClearBufferfv(GL_COLOR, 0, black);
    // glBindVertexArray(VAOs[Cubes]);
    glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_SHORT, NULL);
    // glDrawElementsInstanced(GL_TRIANGLES, 36, GL_UNSIGNED_SHORT, NULL, 1);
    glFlush();
}

void input(GLFWwindow *window, int key, int scancode, int action, int mods)
{
    if (action == GLFW_PRESS) {
        switch (key) {
        case GLFW_KEY_ESCAPE:
            glfwSetWindowShouldClose(window, 1);
            break;
        default:
            break;
        }
    }
}

int main(void)
{
    // setWindowSize(812, 812);
    createWindow("Cube", NULL, NULL, init, display, input);
    return 0;
}
