#ifndef WINDOW_H_
#define WINDOW_H_

#define GLFW_INCLUDE_NONE
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <stdio.h>
#include <stdlib.h>

static const float black[] = {0.0f, 0.0f, 0.0f, 0.0f};

void createWindow(const char *title, GLFWmonitor *monitor, GLFWwindow *share,
                  void (*init)(void), void (*display)(void),
                  void (*input)(GLFWwindow *window, int key, int scancode,
                                int action, int mods));
void setWindowSize(int width, int height);
int  getWidth();
int  getHeight();
#endif // WINDOW_H_
