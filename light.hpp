#ifndef LIGHT_H_
#define LIGHT_H_

#include <glm/glm.hpp>

class Light
{
  public:
    typedef struct {
        bool      isEnabled;
        bool      isLocal;
        bool      isSpot;
        glm::vec3 ambient;
        glm::vec3 color;
        glm::vec3 position;
        glm::vec3 halfVector;
        glm::vec3 coneDirection;
        float     spotCosCutoff;
        float     spotExponent;
        float     constantAttenuation;
        float     linearAttenuation;
        float     quadraticAttenuation;
    } LightProperties;

    typedef struct {
        glm::vec3 emission;
        glm::vec3 ambient;
        glm::vec3 diffuse;
        glm::vec3 specular;
        float     shininess;
    } MaterialProperties;
#endif // LIGHT_H_
