#include "shader.h"
#include "window.h"
#include <GLFW/glfw3.h>
#include <cstddef>
#include <glm/ext/matrix_transform.hpp>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <stdlib.h>
#include <time.h>

#define NumVertices 8
#define WIDTH       812
#define HEIGHT      812

enum VAO_IDs { Cube, Light, NumVAOs };
enum Buffer_IDs { ArrayBuffer, IndexBuffer, NumBuffers };
enum Attrib_IDs { vPosition, vColor, vNormal };

GLuint VAOs[NumVAOs];
GLuint Buffers[NumBuffers];
GLuint program;

float angle = 0.0;

void setModelViewProjection(void)
{
    GLuint mvpMatrixUnif = glGetUniformLocation(program, "mvpMatrix");

    glm::mat4 model = glm::mat4(1.0); // initialize as identity matrix
    glm::mat4 view = glm::lookAt(glm::vec3(2.0, 2.0, 2.0),  // camera position
                                 glm::vec3(0.0, 0.0, 0.0),  // camera target
                                 glm::vec3(0.0, 1.0, 0.0)); // up vector
    glm::mat4 projection = glm::perspective(glm::radians(45.0), // field of view
                                            1.0,                // aspect ratio
                                            0.1,                // near plane
                                            100.0);             // far plane

    glm::mat4 matrix = projection * view * model;

    glUniformMatrix4fv(mvpMatrixUnif, 1.0, GL_FALSE, glm::value_ptr(matrix));
}

void setLight(void)
{
    GLuint lightPositionUnif = glGetUniformLocation(program, "light.position");
    GLuint lightAmbientUnif = glGetUniformLocation(program, "light.ambient");
    GLuint lightDiffuseUnif = glGetUniformLocation(program, "light.diffuse");
    GLuint lightSpecularUnif = glGetUniformLocation(program, "light.specular");

    GLuint materialAmbientUnif =
        glGetUniformLocation(program, "material.ambient");
    GLuint materialDiffuseUnif =
        glGetUniformLocation(program, "material.diffuse");
    GLuint materialSpecularUnif =
        glGetUniformLocation(program, "material.specular");
    GLuint materialShininessUnif =
        glGetUniformLocation(program, "material.shininess");

    GLuint viewPosUnif = glGetUniformLocation(program, "viewPos");

    glm::vec3 lightColor = glm::vec3(1.0f);
    glm::vec3 diffuseColor = lightColor * glm::vec3(0.99f);
    glm::vec3 ambientColor = diffuseColor * glm::vec3(0.35f);

    glUniform3f(viewPosUnif, 2.0f, 2.0f, 2.0f);
    glUniform3f(lightPositionUnif, 0.0f, -2.0f, 0.0f);
    glUniform3fv(lightAmbientUnif, 1, &ambientColor[0]);
    glUniform3fv(lightDiffuseUnif, 1, &diffuseColor[0]);
    glUniform3f(lightSpecularUnif, 1.0f, 1.0f, 1.0f);

    glUniform3f(materialAmbientUnif, 0.4f, 0.5f, 0.31f);
    glUniform3f(materialDiffuseUnif, 1.0f, 0.5f, 0.31f);
    glUniform3f(materialSpecularUnif, 0.75f, 0.75f, 0.75f);
    glUniform1f(materialShininessUnif, 80.0f);
}

void rotateCube(void)
{
    angle += 10.0f;
    GLuint    transMatrixUnif = glGetUniformLocation(program, "transMatrix");
    glm::mat4 rotation = glm::rotate(glm::mat4(1.0), glm::radians(angle),
                                     glm::vec3(0.0, 1.0, 0.0));
    glUniformMatrix4fv(transMatrixUnif, 1.0, GL_FALSE,
                       glm::value_ptr(rotation));
}

void setRandomColor(void)
{
    srand(time(NULL));
    VertexData *bufferData = (VertexData *)glMapNamedBufferRange(
        Buffers[ArrayBuffer], 0, NumVertices * sizeof(VertexData),
        GL_MAP_WRITE_BIT);
    for (int i = 0; i < NumVertices; i++) {
        bufferData[i].color[0] = rand() % 256;
        bufferData[i].color[1] = rand() % 256;
        bufferData[i].color[2] = rand() % 256;
    }
    glUnmapNamedBuffer(Buffers[ArrayBuffer]);
}

void init(void)
{
    ShaderInfo shaders[] = {
        {GL_VERTEX_SHADER,   "cube.vert"},
        {GL_FRAGMENT_SHADER, "cube.frag"},
        {GL_NONE,            NULL       }
    };
    program = loadShaders(shaders);
    glUseProgram(program);

    glGenVertexArrays(NumVAOs, VAOs);
    glBindVertexArray(VAOs[Cube]);

    VertexData vertices[NumVertices] = {
        {{-0.5f, -0.5f, 0.5f},  {255, 255, 255, 255}, {}, {1.0f, 0.0f, 0.0f} },
        {{0.5f, -0.5f, 0.5f},   {128, 127, 110, 255}, {}, {1.0f, 0.0f, 0.0f} },
        {{0.5f, 0.5f, 0.5f},    {90, 50, 255, 255},   {}, {1.0f, 0.0f, 0.0f} },
        {{-0.5, 0.5f, 0.5f},    {255, 255, 255, 255}, {}, {1.0f, 0.0f, 0.0f} },

        {{-0.5f, -0.5f, -0.5f}, {40, 40, 40, 255},    {}, {-1.0f, 0.0f, 0.0f}},
        {{0.5f, -0.5f, -0.5f},  {100, 90, 80, 255},   {}, {-1.0f, 0.0f, 0.0f}},
        {{0.5f, 0.5f, -0.5f},   {70, 80, 90, 255},    {}, {-1.0f, 0.0f, 0.0f}},
        {{-0.5f, 0.5f, -0.5f},  {124, 125, 126, 255}, {}, {-1.0f, 0.0f, 0.0f}}
    };

    GLushort indices[36] = {0, 1, 2, 2, 3, 0, 4, 5, 6, 6, 7, 4,
                            3, 2, 6, 6, 7, 3, 0, 1, 5, 5, 4, 0,
                            1, 2, 6, 6, 5, 1, 0, 3, 7, 7, 4, 0};

    glCreateBuffers(NumBuffers, Buffers);
    setImmuBufferObject(Buffers[ArrayBuffer], GL_ARRAY_BUFFER, sizeof(vertices),
                        vertices, GL_MAP_WRITE_BIT);
    setImmuBufferObject(Buffers[IndexBuffer], GL_ELEMENT_ARRAY_BUFFER,
                        sizeof(indices), indices, GL_MAP_READ_BIT);

    setVertexAttribute(Buffers[ArrayBuffer], ArrayBuffer, vPosition, 3,
                       GL_FLOAT, GL_TRUE, sizeof(VertexData), 0,
                       offsetof(VertexData, position));
    setVertexAttribute(Buffers[ArrayBuffer], ArrayBuffer, vColor, 4,
                       GL_UNSIGNED_BYTE, GL_TRUE, sizeof(VertexData), 0,
                       offsetof(VertexData, color));
    setVertexAttribute(Buffers[ArrayBuffer], ArrayBuffer, vNormal, 3, GL_FLOAT,
                       GL_FALSE, sizeof(VertexData), 0,
                       offsetof(VertexData, normal));

    setModelViewProjection();
    setLight();

    glBindVertexArray(VAOs[Cube]);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    glDisable(GL_CULL_FACE);
    glCullFace(GL_BACK);
    glFrontFace(GL_CW);
    glEnable(GL_NORMALIZE);
    glShadeModel(GL_FLAT);
    glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_FALSE);
    glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER, GL_TRUE);
}

void display(void)
{
    double time = glfwGetTime();
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glClearBufferfv(GL_COLOR, 0, black);
    glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_SHORT, NULL);
    if (time >= 0.1) {
        setRandomColor();
        rotateCube();
        glfwSetTime(0.0);
    }
    glFlush();
}

void input(GLFWwindow *window, int key, int scancode, int action, int mods)
{
    if (action == GLFW_PRESS) {
        switch (key) {
        case GLFW_KEY_ESCAPE:
            glfwSetWindowShouldClose(window, 1);
            break;
        default:
            break;
        }
    }
}

int main(void)
{
    setWindowSize(WIDTH, HEIGHT);
    createWindow("Cube", NULL, NULL, init, display, input);
    return 0;
}
