#ifndef SHADER_H_
#define SHADER_H_

#include <GL/glew.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_NUM_LINES    10000UL
#define MAX_NUM_CHARS    (MAX_NUM_LINES * 200UL)
#define BUFFER_OFFSET(a) ((void *)(a))

typedef struct {
    GLenum      type;
    const char *filename;
    GLuint      shader;
} ShaderInfo;

typedef struct {
    GLfloat position[3];
    GLubyte color[4];
    GLubyte texture[2];
    GLfloat normal[3];
} VertexData;

GLuint loadShaders(ShaderInfo *shader);
void   setVertexAttribute(GLuint vbo, GLuint binding_index, GLuint attrib_index,
                          GLuint size, GLenum type, GLboolean normalized,
                          GLsizei stride, GLintptr offset, GLuint relativeOffset);
void   setBufferObject(GLuint vbo, GLenum target, GLsizeiptr data_size,
                       const void *data, GLenum usage);
void   setImmuBufferObject(GLuint vbo, GLenum target, GLsizeiptr data_size,
                           const void *data, GLbitfield flags);
#endif // SHADER_H_
