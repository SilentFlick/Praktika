#include "LogFileHandler.hpp"
#include <osgDB/ReadFile>
#include <osgViewer/Viewer>

int main(int argc, char **argv)
{
    osg::setNotifyLevel(osg::INFO);
    osg::setNotifyHandler(new LogFileHandler("output.log"));
    osg::ArgumentParser arguments(&argc, argv);
    std::string         filename;
    arguments.read("--model", filename);
    osg::ref_ptr<osg::Node> root = osgDB::readNodeFile(filename);
    if (!root) {
        OSG_FATAL << arguments.getApplicationName() << ": No data loaded."
                  << std::endl;
        return -1;
    }
    osgViewer::Viewer viewer;
    viewer.setSceneData(root.get());
    return viewer.run();
}
