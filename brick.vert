#version 450 core

layout(location = 0) in vec3 vPosition;

uniform mat4 mvpMatrix;
uniform mat4 transMatrix;

out vec3  MCPosition;

void main()
{
    vec4 position = vec4(vPosition, 1.0);
    MCPosition = vPosition;
    gl_Position = mvpMatrix * transMatrix * position;
}
