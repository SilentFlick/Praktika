#version 450 core

layout(location = 0) in vec3 vPosition;
layout(location = 1) in vec4 vColor;
layout(location = 2) in vec3 vNormal;

uniform vec3 offset;
uniform mat4 mvpMatrix;
uniform mat4 transMatrix;
uniform mat3 normalMatrix;
uniform int  materialIndex;

out vec4     color;
out vec3     normal;
out vec3     position;
flat out int MatIndex;

void main()
{
    vec4 offsetPos = vec4(vPosition, 1.0) + vec4(offset, 0.0);
    normal = normalize(normalMatrix * vNormal);
    color = vColor;
    position = vec3(mvpMatrix * vec4(vPosition, 1.0));
    MatIndex = materialIndex;
    gl_Position = mvpMatrix * transMatrix * offsetPos;
}
