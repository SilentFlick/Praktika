#version 450 core

struct LightProperties {
    bool  isEnabled;     // true to apply this light
    bool  isLocal;       // true for a point light or a spotlight, false for a
                         // positional light
    bool  isSpot;        // true if the light is a spotlight
    vec3  ambient;       // light's contribution to ambient light
    vec3  color;         // color of light
    vec3  position;      // location of light, if isLocal is true, otherwise the
                         // direction toward the light
    vec3  halfVector;    // direction of highlights for directional light
    vec3  coneDirection; // spotlight attributes
    float spotCosCutoff;
    float spotExponent;
    float constantAttenuation; // local light attenuation coefficients
    float linearAttenuation;
    float quadraticAttenuation;
};

struct MaterialProperties {
    vec3  emission;  // light produced by the material
    vec3  ambient;   // what part of ambient light is reflected
    vec3  diffuse;   // what part of diffuse light is scattered
    vec3  specular;  // what part of specular light is scattered
    float shininess; // exponent for sharpening specular reflection
};

// a set of materials to select between, per shader invocation
const int                  MaxMaterials = 14;
// use even indexes for front-facing surfaces and odd indexes for back facing
uniform MaterialProperties Material[2 * MaxMaterials];
flat in int                MatIndex; // input material index from vertex shader

// the set of lights to apply, per invocation of this shader
const int               MaxLights = 10;
uniform LightProperties Lights[MaxLights];
uniform float           shininess;
uniform float           shininessStrength;
uniform vec3            eyeDirection;

in vec4 color;
in vec3 normal; // surface normal, interpolated between vertices
in vec4 position;

layout(location = 0) out vec4 fColor;

void main()
{
    int mat;
    if (gl_FrontFacing) {
        mat = MatIndex;
    } else {
        mat = MatIndex + 1;
    }
    vec3 scatteredLight = vec3(0.0); // or to a global ambient light
    vec3 reflectedLight = vec3(0.0);
    // loop over all the lights
    for (int light = 0; light < MaxLights; ++light) {
        if (!Lights[light].isEnabled) {
            continue;
        }
        vec3  halfVector;
        vec3  lightDirection = Lights[light].position;
        float attenuation = 1.0;
        // for local lights, compute per-fragment direction, halfVector, and
        // attenuation
        if (Lights[light].isLocal) {
            lightDirection = lightDirection - vec3(position);
            float lightDistance = length(lightDirection);
            lightDirection = lightDirection / lightDistance;
            attenuation =
                1.0 / (Lights[light].constantAttenuation +
                       Lights[light].linearAttenuation * lightDistance +
                       Lights[light].quadraticAttenuation * lightDistance *
                           lightDistance);
            if (Lights[light].isSpot) {
                float spotCos =
                    dot(lightDirection, Lights[light].coneDirection);
                if (spotCos < Lights[light].spotCosCutoff)
                    attenuation = 0.0;
                else
                    attenuation *= pow(spotCos, Lights[light].spotExponent);
            }
            halfVector = normalize(lightDirection + eyeDirection);
        } else {
            halfVector = Lights[light].halfVector;
        }
        float diffuse = max(0.0, dot(normal, lightDirection));
        float specular = max(0.0, dot(normal, halfVector));
        if (diffuse == 0.0)
            specular = 0.0;
        else
            specular = pow(specular, shininess) * shininessStrength;
        // accumulate all the lights' effects.
        scatteredLight +=
            Lights[light].ambient * Material[mat].ambient * attenuation +
            Lights[light].color * Material[mat].diffuse * diffuse * attenuation;
        reflectedLight += Lights[light].color * Material[mat].specular *
                          specular * attenuation;
    }
    vec3 rgb = min(Material[mat].emission + color.rgb * scatteredLight +
                       reflectedLight,
                   vec3(1.0));
    fColor = vec4(rgb, color.a);
}
