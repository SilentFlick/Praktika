#include "shader.h"
#include "window.h"

#define NumVertices 6

enum VAO_IDs { Triangles, NumVAOs };
enum Buffer_IDs { ArrayBuffer, IndexBuffer, NumBuffers };
enum Atrrib_IDs { vPosition, vColor };

GLuint VAOs[NumVAOs];
GLuint Buffers[NumBuffers];

void init(void)
{
    ShaderInfo shaders[] = {
        {GL_VERTEX_SHADER,   "triangles.vert"},
        {GL_FRAGMENT_SHADER, "triangles.frag"},
        {GL_NONE,            NULL            }
    };

    GLuint program = loadShaders(shaders);
    glUseProgram(program);

    glGenVertexArrays(NumVAOs, VAOs);
    glBindVertexArray(VAOs[Triangles]);

    // There are  6 vertices, each one has 2 values position and 3 values color.
    GLfloat vertices[NumVertices][5] = {
        {-0.90, -0.90, 1.0, 0.0, 0.0  },
        {0.85,  -0.90, 0.0, 1.0, 0.0  },
        {-0.90, 0.85,  0.0, 0.0, 1.0  }, // Triangle 1
        {0.90,  -0.85, 0.0, 0.0, 255.0},
        {0.90,  0.90,  0.0, 0.0, 255.0},
        {-0.85, 0.90,  0.0, 0.0, 255.0}  // Triangle 2
    };

    GLuint indices[NumVertices] = {0, 1, 2, 3, 4, 5};

    glCreateBuffers(NumBuffers, Buffers);
    setBufferObject(Buffers[ArrayBuffer], GL_ARRAY_BUFFER, sizeof(vertices),
                    vertices, GL_STATIC_DRAW);
    setBufferObject(Buffers[IndexBuffer], GL_ELEMENT_ARRAY_BUFFER,
                    sizeof(indices), indices, GL_STATIC_DRAW);
    // Solution 1
    /* glVertexAttribPointer(vPosition, 2, GL_FLOAT, GL_FALSE, 0,
     * BUFFER_OFFSET(0)); */
    /* glEnableVertexAttribArray(vPosition); */

    // Solution 2 - Better!
    /* glBindVertexBuffer(0, Buffers[ArrayBuffer], 0, 2 * sizeof(GL_FLOAT)); */
    /* glVertexAttribFormat(vPosition, 2, GL_FLOAT, GL_FALSE, 0); */
    /* glVertexAttribBinding(vPosition, 0); */
    /* glEnableVertexAttribArray(vPosition); */
    setVertexAttribute(Buffers[ArrayBuffer], ArrayBuffer, vPosition, 2,
                       GL_FLOAT, GL_FALSE, 5 * sizeof(GL_FLOAT), 0,
                       0); // Position
    setVertexAttribute(Buffers[ArrayBuffer], ArrayBuffer, vColor, 3, GL_FLOAT,
                       GL_FALSE, 5 * sizeof(GL_FLOAT), 0,
                       2 * sizeof(GL_FLOAT)); // Color
}

void display(void)
{
    glClearBufferfv(GL_COLOR, 0, black);
    glBindVertexArray(VAOs[Triangles]);
    /* glVertexAttrib3f(vColor, 1, 0, 0); */
    /* glDrawArrays(GL_TRIANGLES, 0, NumVertices); */
    glDrawElements(GL_TRIANGLES, NumVertices, GL_UNSIGNED_INT, 0);
}

void input(GLFWwindow *window, int key, int scancode, int action, int mods)
{
    if (action == GLFW_PRESS) {
        switch (key) {
        case GLFW_KEY_ESCAPE:
            glfwSetWindowShouldClose(window, 1);
            break;
        default:
            break;
        }
    }
}

int main(int argc, char *argv[])
{
    /* glutInit(&argc, argv); */
    /* glutInitDisplayMode(GLUT_RGBA | GLUT_DEPTH); */
    /* glutInitWindowSize(512, 512); */
    /* glutCreateWindow("Triangles"); */
    /* if (glewInit()) { */
    /*     perror("glewInit"); */
    /*     exit(EXIT_FAILURE); */
    /* } */
    /* init(); */
    /* glutDisplayFunc(display); */
    /* glutMainLoop(); */
    setWindowSize(500, 500);
    createWindow("Triangles", NULL, NULL, init, display, input);
    return 0;
}
