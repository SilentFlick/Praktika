#include "shader.h"
#include "window.h"
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#define NumVertices 4

enum VAO_IDs { Rectangle, NumVAOs };
enum Buffer_IDs { ArrayBuffer, IndexBuffer, NumBuffers };
enum Attrib_IDs { vPosition, vColor, vTexture };
enum Texture_IDs { Texture1, NumTextures };

GLuint VAOs[NumVAOs];
GLuint Buffers[NumBuffers];
GLuint Textures[NumTextures];
GLuint program;

void init(void)
{
    ShaderInfo shaders[] = {
        {GL_VERTEX_SHADER,   "rectangle.vert"},
        {GL_FRAGMENT_SHADER, "rectangle.frag"},
        {GL_NONE,            NULL            }
    };
    program = loadShaders(shaders);
    glUseProgram(program);

    VertexData vertices[NumVertices] = {
  //     positions             colors                texture coords
        {{0.5f, 0.5f, 0.0f},   {255, 255, 255, 255}, {1, 1}},
        {{0.5f, -0.5f, 0.0f},  {255, 0, 0, 255},     {1, 0}},
        {{-0.5f, -0.5f, 0.0f}, {0, 255, 0, 255},     {0, 0}},
        {{-0.5f, 0.5f, 0.0f},  {0, 0, 255, 255},     {0, 1}}
    };
    unsigned int indices[] = {0, 1, 3, 1, 2, 3};
    glGenVertexArrays(NumVAOs, VAOs);
    glBindVertexArray(VAOs[Rectangle]);

    glCreateBuffers(NumBuffers, Buffers);
    setImmuBufferObject(Buffers[ArrayBuffer], GL_ARRAY_BUFFER, sizeof(vertices),
                        vertices, GL_MAP_WRITE_BIT);
    setImmuBufferObject(Buffers[IndexBuffer], GL_ELEMENT_ARRAY_BUFFER,
                        sizeof(indices), indices, GL_MAP_READ_BIT);

    glGenTextures(NumTextures, Textures);
    glBindTexture(GL_TEXTURE_2D, Textures[Texture1]);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    setVertexAttribute(Buffers[ArrayBuffer], ArrayBuffer, vPosition, 3,
                       GL_FLOAT, GL_TRUE, sizeof(VertexData), 0,
                       offsetof(VertexData, position));
    setVertexAttribute(Buffers[ArrayBuffer], ArrayBuffer, vColor, 4,
                       GL_UNSIGNED_BYTE, GL_TRUE, sizeof(VertexData), 0,
                       offsetof(VertexData, color));
    setVertexAttribute(Buffers[ArrayBuffer], ArrayBuffer, vTexture, 2,
                       GL_UNSIGNED_BYTE, GL_FALSE, sizeof(VertexData), 0,
                       offsetof(VertexData, texture));

    int            width, height, nrChannels;
    unsigned char *data =
        stbi_load("./media/container.jpg", &width, &height, &nrChannels, 0);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB,
                 GL_UNSIGNED_BYTE, data);
    glGenerateMipmap(GL_TEXTURE_2D);

    glBindVertexArray(VAOs[Rectangle]);
}

void display(void)
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glClearBufferfv(GL_COLOR, 0, black);
    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, NULL);
    glFlush();
}

void input(GLFWwindow *window, int key, int scancode, int action, int mods)
{
    if (action == GLFW_PRESS) {
        switch (key) {
        case GLFW_KEY_ESCAPE:
            glfwSetWindowShouldClose(window, 1);
            break;
        default:
            break;
        }
    }
}

int main(void)
{
    // setWindowSize(812, 812);
    createWindow("Rectangle", NULL, NULL, init, display, input);
    return 0;
}
