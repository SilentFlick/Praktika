#include "shader.h"

void readShader(const char *filePath, char *content);
void deleteShaders(ShaderInfo *shaders);
int  checkCompileErrors(unsigned int shader);
int  checkLinkErrors(unsigned int program);

GLuint loadShaders(ShaderInfo *shaders)
{
    if (shaders == NULL) {
        return 0;
    }
    // We prepare a program to compile and link all shaders, an array to store
    // the shader source code and a pointer points to this array.
    GLuint      program = glCreateProgram();
    ShaderInfo *entry = shaders;
    char        shaderSource[MAX_NUM_CHARS];
    const char *pointerSource = shaderSource;

    while (entry->type != GL_NONE) {
        // Now we create a shader from type.
        entry->shader = glCreateShader(entry->type);
        // Read shader file and write to array.
        readShader(entry->filename, shaderSource);
        if (pointerSource == NULL) {
            deleteShaders(shaders);
            printf("Shader file '%s' is empty.\n", entry->filename);
            return 0;
        }
        // Compile shader and check if errors exist.
        glShaderSource(entry->shader, 1, &pointerSource, NULL);
        glCompileShader(entry->shader);
        if (checkCompileErrors(entry->shader)) {
            deleteShaders(shaders);
            return 0;
        }
        glAttachShader(program, entry->shader);
        printf("Compiled '%s'\n", entry->filename);
        // Now we fill the shader array with 0, so that it can store the next
        // shader code.
        memset(&shaderSource[0], 0, sizeof(shaderSource));
        ++entry;
    }
    glLinkProgram(program);
    // We delete all shaders because our program already has a copy.
    deleteShaders(shaders);
    if (checkLinkErrors(program)) {
        return 0;
    }
    return program;
}

void readShader(const char *filePath, char *content)
{
    FILE *fp;
    int   c;
    char *pointer;

    pointer = content;
    if ((fp = fopen(filePath, "rb")) == NULL) {
        perror("fopen");
        exit(EXIT_FAILURE);
    }
    while ((c = fgetc(fp)) != EOF) {
        *pointer = c;
        *(++pointer) = 0;
    }
    fclose(fp);
}

void deleteShaders(ShaderInfo *shaders)
{
    ShaderInfo *entry;
    for (entry = shaders; entry->type != GL_NONE; ++entry) {
        glDeleteShader(entry->shader);
        entry->shader = 0;
    }
}

int checkCompileErrors(unsigned int shader)
{
    int success;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
    if (!success) {
        char log[1025];
        glGetShaderInfoLog(shader, 1024, NULL, log);
        printf("ERROR: SHADER_COMPILATION_ERROR: '%s'\n", log);
        return 1;
    }
    return 0;
}

int checkLinkErrors(unsigned int program)
{
    int success;
    glGetProgramiv(program, GL_LINK_STATUS, &success);
    if (!success) {
        char log[1025];
        glGetShaderInfoLog(program, 1024, NULL, log);
        printf("ERROR: SHADER_LINKING_ERROR '%s'\n", log);
        return 1;
    }
    return 0;
}

/* @Param vbo: the buffer object
 * @Param binding_index: the vertex buffer binding index to bind the VBO to.
 * @Param attrib_index: the index of the vertex attribute to set up.
 * @Param size: the number of components in the attribute (e.g. 2 for a 2D
 * position)
 * @Param type: the data type of the attribute components (e.g. GL_FLOAT).
 * @Param normalized: a boolean flag indicating whether the attribute values
 * should be normalized.
 * @Param stride: the byte offset between consecutive vertices in the VBO.
 * @Param offset: the offset of the first element within the buffer
 * @Param relativeOffset: the offset of the data of the vertice in the buffer */
void setVertexAttribute(GLuint vbo, GLuint binding_index, GLuint attrib_index,
                        GLuint size, GLenum type, GLboolean normalized,
                        GLsizei stride, GLintptr offset, GLuint relativeOffset)
{
    glBindVertexBuffer(binding_index, vbo, offset, stride);
    glVertexAttribFormat(attrib_index, size, type, normalized, relativeOffset);
    glVertexAttribBinding(attrib_index, binding_index);
    glEnableVertexAttribArray(attrib_index);
}

/* @Param vbo: the buffer object
 * @Param target: the target which the buffer object is bound
 * @Param data_size: the size in bytes of the buffer object's new data source
 * @Param data: a pointer to data that will be copied into the data store
 * @Param flags: the intended usage of the buffer's data store */
void setBufferObject(GLuint vbo, GLenum target, GLsizeiptr data_size,
                     const void *data, GLenum usage)
{
    glBindBuffer(target, vbo);
    glBufferData(target, data_size, data, usage);
}

void setImmuBufferObject(GLuint vbo, GLenum target, GLsizeiptr data_size,
                         const void *data, GLbitfield flags)
{

    glBindBuffer(target, vbo);
    glBufferStorage(target, data_size, data, flags);
}
