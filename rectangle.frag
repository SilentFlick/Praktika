#version 330 core

in vec4 color;
in vec2 texCoord;

uniform sampler2D texture1;

layout( location = 0 )out vec4 FragColor;

void main()
{
    FragColor = texture(texture1, texCoord) * color;
}
