#ifndef LOGFILEHANDLER_H_
#define LOGFILEHANDLER_H_

#include <fstream>
#include <osgDB/ReadFile>
#include <osgViewer/Viewer>

class LogFileHandler : public osg::NotifyHandler
{
  public:
    LogFileHandler(const std::string &file) { _log.open(file.c_str()); }
    virtual ~LogFileHandler() { _log.close(); }
    virtual void notify(osg::NotifySeverity severity, const char *msg)
    {
        _log << msg;
    }

  protected:
    std::ofstream _log;
};
#endif // LOGFILEHANDLER_H_
