#include <osg/Geode>
#include <osg/Geometry>
#include <osgViewer/Viewer>

int main(void)
{
    osg::ref_ptr<osg::Vec3Array> vertices = new osg::Vec3Array(4);
    // Create the vertex array and push the four corner points to the back of
    // the array
    (*vertices)[0].set(0.0f, 0.0f, 0.0f);
    (*vertices)[1].set(1.0f, 0.0f, 0.0f);
    (*vertices)[2].set(1.0f, 0.0f, 1.0f);
    (*vertices)[3].set(0.0f, 0.0f, 1.0f);

    // Indicate the normal of each vertex, so that the lighting calculation will
    // be correct. The four vertices face the same direction, so a single normal
    // vector is enough
    osg::ref_ptr<osg::Vec3Array> normals = new osg::Vec3Array(1);
    (*normals)[0].set(0.0f, -1.0f, 0.0f);

    // Indicate a unique color value to each vertex
    osg::ref_ptr<osg::Vec4Array> colors = new osg::Vec4Array(4);
    (*colors)[0].set(1.0f, 0.0f, 0.0f, 1.0f);
    (*colors)[1].set(0.0f, 1.0f, 0.0f, 1.0f);
    (*colors)[2].set(0.0f, 0.0f, 1.0f, 1.0f);
    (*colors)[3].set(1.0f, 1.0f, 1.0f, 1.0f);

    // Indexing primitives
    osg::ref_ptr<osg::DrawElementsUInt> indices =
        new osg::DrawElementsUInt(GL_TRIANGLES, 6);
    // First triangle
    (*indices)[0] = 0;
    (*indices)[1] = 1;
    (*indices)[2] = 2;
    // Second triangle
    (*indices)[3] = 0;
    (*indices)[4] = 2;
    (*indices)[5] = 3;

    osg::ref_ptr<osg::Geometry> quad = new osg::Geometry;
    quad->setVertexArray(vertices.get());
    quad->setNormalArray(normals.get());
    quad->setNormalBinding(osg::Geometry::BIND_OVERALL);
    quad->setColorArray(colors.get());
    quad->setColorBinding(osg::Geometry::BIND_PER_VERTEX);
    // Specify the primitive set
    quad->addPrimitiveSet(indices.get());

    osg::ref_ptr<osg::Geode> root = new osg::Geode;
    root->addDrawable(quad.get());

    osgViewer::Viewer viewer;
    viewer.setSceneData(root.get());
    return viewer.run();
}
